<?php

namespace Danmer\Photoreact\Controller;

use YOOtheme\Framework\Routing\Controller;
use YOOtheme\Framework\Routing\Exception\HttpException;

class ContentController extends Controller
{
    public function indexAction()
    {
        return $this['view']->render('views/content.php');
    }

    public function queryContentAction()
    {
        $contents = new \ArrayObject;

        foreach ($this['content']->findAll() as $id => $content) {

            if (isset($this['types'][$content->getType()])) {
                $contents[$id] = $content->toArray();
            }
        }

        return $this['response']->json($contents);
    }

    public function getContentAction($id)
    {
        if ($content = $this['content']->find($id)) {
            return $this['response']->json($content->toArray());
        }

        throw new HttpException(404);
    }

    public function saveContentAction($content)
    {
        $status = !isset($content['id']) || !$content['id'] ? 201 : 200;
        if ($content = $this['content']->save($content)) {
            return $this['response']->json($content, $status);
        }

        throw new HttpException(400);
    }

  public function saveConfigAction($config)
  {
    if($this['calculator']->saveData($config)){
      return $this['response']->json($config, 200);
    }
    throw new HttpException(400);
  }

  public function getConfigAction()
  {
      return $this['response']->json($this['calculator']->get(), 200);
  }

    public function deleteContentAction($id)
    {
        if ($this['content']->delete($id)) {
            return $this['response']->json(null, 204);
        }

        throw new HttpException(400);
    }

    public static function getRoutes()
    {
        return [
          ['index', 'indexAction', 'GET', ['access' => 'manage_photoreact']],
          ['/content', 'queryContentAction', 'GET', ['access' => 'manage_photoreact']],
          ['/content/:id', 'getContentAction', 'GET', ['access' => 'manage_photoreact']],
          ['/content(/:id)', 'saveContentAction', 'POST', ['access' => 'manage_photoreact']],
          ['/calc', 'getConfigAction', 'GET', ['access' => 'manage_photoreact']],
          ['/calc', 'saveConfigAction', 'POST', ['access' => 'manage_photoreact']],
          ['/content/:id', 'deleteContentAction', 'DELETE', ['access' => 'manage_photoreact']]
        ];
    }
}
