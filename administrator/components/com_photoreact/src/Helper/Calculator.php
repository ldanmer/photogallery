<?php


namespace Danmer\Photoreact\Helper;

use Exception as PhotoreactException;
use JsonException;

/**
 * Class Calculator
 * @package Danmer\Photoreact\Helper
 */

class Calculator
{
  private $fileName;
  private $filePath;
  const SAVE_DIR = JPATH_ROOT . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_photoreact' . DIRECTORY_SEPARATOR;

  /**
   * Calculator constructor.
   * @param $fileName
   * @throws PhotoreactException
   */
  public function __construct($fileName)
  {
    $this->fileName = $fileName;
    $this->filePath = $this->setFilePath();
  }

  /**
   * Saves calculator config
   *
   * @param array $data
   * @return bool
   * @throws JsonException
   */
  public function saveData(array $data)
  {
      if($jsonData = json_encode($data)){
        return file_put_contents($this->filePath, $jsonData, LOCK_EX) > 0;
      }
    throw new JsonException('cannot encode json data');
  }

  /**
   * @return array
   * @throws JsonException
   */
  public function getData(){
    if($jsonData = file_get_contents($this->filePath)){
      return json_decode($jsonData);
    }
    throw new JsonException('cannot encode json data');
  }

  /**
   * @return string
   * @throws PhotoreactException
   */
  private function setFilePath()
  {
    if (!is_dir(self::SAVE_DIR) && !mkdir(self::SAVE_DIR, 0777, true)) {
      throw new PhotoreactException('cannot create the dir');
    }
    return self::SAVE_DIR . $this->fileName;
  }

}