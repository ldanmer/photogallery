<?php

namespace Danmer\Photoreact;

interface Update
{
    /**
     * Executes the update.
     */
    public function run();
}
