<?php

namespace Danmer\Photoreact;

use Danmer\Photoreact\Content\ContentProvider;
use Danmer\Photoreact\Helper\Calculator;
use Danmer\Photoreact\Helper\Shortcode;
use Danmer\Photoreact\Image\ImageProvider;
use YOOtheme\Framework\Application as BaseApplication;
use YOOtheme\Framework\Event\EventSubscriberInterface;

class Application extends BaseApplication implements EventSubscriberInterface
{
  const REGEX_URL = '/
                        (?P<attr>href|src|poster)=             # match the attribute
                        ([\"\'])                               # start with a single or double quote
                        (?!\/|\#|(mailto|news|(ht|f)tp(s?))\:) # make sure it is a relative path
                        (?P<url>[^\"\'>]+)                     # match the actual src value
                        \2                                     # match the previous quote
                       /xiU';

  /**
   * Constructor.
   *
   * @param array $values
   * @throws \Exception
   */
  public function __construct(array $values = array())
  {
    parent::__construct($values);

    $this['content'] = new ContentProvider($this);
    $this['image'] = new ImageProvider($this);
    $this['shortcode'] = new Shortcode;
    $this['types'] = new Collection;
    $this['widgets'] = new Collection;
    $this['calculator'] = new Calculator('calculator.json');

    $this->extend('locator', function ($locator, $app) {
      return $locator->addPath('', $app['path']);
    });


    $this->extend('translator', function ($translator, $app) {
      return $translator->addResource('languages/' . $app['locale'] . '.json');
    });

    $this->on('boot', function ($event, $app) {

      $app['plugins']->addPath($app['path'] . '/plugins/*/*/plugin.php');

      foreach ($app['templates'] as $path) {
        $app['locator']->addPath('plugins', $path);
        $app['plugins']->addPath($path . '/*/*/plugin.php');
      }

    });

    $this['events']->addSubscriber($this);
  }

  public function init()
  {
    // controller
    $this['controllers']->add('Danmer\Photoreact\Controller\ContentController');
    $this['controllers']->add('Danmer\Photoreact\Controller\ImageController');

    // combine assets
    if (!$this['debug']) {
      $this['styles']->combine('styles', 'photoreact-*', array('CssImportResolver', 'CssRewriteUrl', 'CssImageBase64'));
      $this['scripts']->combine('scripts', 'photoreact-*')->combine('uikit', 'uikit*')->combine('angular', 'angular*')->combine('application', 'application{,-translator,-templates}');
    }

    // site event
    if (!$this['admin']) {
      $this->trigger('init.site', array($this));
    }
  }

  public function initSite()
  {
    // styles
    $this->on('view', function ($event, $app) {
      if (!$app['config']->get('theme.support')) {
        $app['styles']->add('photoreact-site', 'assets/css/site.css');
      }
    });

    // scripts
    $this['scripts']->register('uikit', 'vendor/assets/uikit/js/uikit.min.js');
  }

  public function initAdmin()
  {
    // angular
    $this['angular']->set('name', 'photoreact');
    $this['angular']->set('types', $this['types']->toArray());
    $this['angular']->set('widgets', $this['widgets']->toArray());
    $this['angular']->set('images', [
      'audio'       => $this['url']->to('assets/images/preview-audio.svg'),
      'video'       => $this['url']->to('assets/images/preview-video.svg'),
      'iframe'      => $this['url']->to('assets/images/preview-iframe.svg'),
      'placeholder' => $this['url']->to('assets/images/preview-placeholder.svg')
    ]);
    $this['angular']->set('rooms', [
      'Гостиная',
      'Спальня',
      'Детская',
      'Кухня',
      'Прихожая',
      'Санузел',
    ]);

    $this['angular']->set('texture', [
      'Матовые',
      'Глянцевые',
      'Сатиновые',
      'Дизайнерские',
    ]);

    $this['angular']->set('feature', [
      'С подсветкой',
      'Двухуровневые',
      'Фотопечать',
    ]);

    $this['angular']->addTemplate('picker', 'views/picker.php', true);
    $this['angular']->set('calculator', $this['calculator']->getData());

    // photoreact
    $this['styles']->add('photoreact-admin', 'assets/css/admin.css');
    $this['scripts']->add('photoreact-fields', 'assets/js/fields.js', array('angular'));
    $this['scripts']->add('photoreact-application', 'assets/js/application.js', array('uikit', 'uikit-notify', 'uikit-nestable', 'uikit-sortable', 'application-translator', 'angular-resource', 'photoreact-fields'));

    $this['scripts']->add('photoreact-controllers', 'assets/js/controllers.js', array('photoreact-application'));
    $this['scripts']->add('photoreact-directives', 'assets/js/directives.js', array('photoreact-application'));
    $this['scripts']->add('photoreact-environment', 'assets/js/environment.js', array('photoreact-application'));
  }

  public function convertUrls($content)
  {
    $url = $this['url'];

    return preg_replace_callback(self::REGEX_URL, function ($matches) use ($url) {

      if (strpos($matches['url'], 'index.php') !== 0) {
        $matches['url'] = $url->to($matches['url']);
      }

      return sprintf('%s="%s"', $matches['attr'], $matches['url']);
    }, $content);
  }

  public function renderWidget(array $attrs)
  {
    if (
      !isset($attrs['id'])
      or !$content = $this['content']->get($attrs['id'])
      or !$data = $content['_widget']
      or !isset($data['name'], $data['data'])
      or !$widget = $this['widgets']->get($data['name'])
    ) {
      return '';
    }

    $data = array_map(function ($value) {
      return in_array($value, array('true', 'false')) ? $value == 'true' : $value;
    }, $data['data']);

    return $this->convertUrls($widget->render($content, $data));
  }

  public function install()
  {
    $sql = "CREATE TABLE IF NOT EXISTS @photoreact (
            id int(10) NOT NULL AUTO_INCREMENT,
            name VARCHAR(255) NOT NULL,
            type VARCHAR(255) NOT NULL,
            data longtext NOT NULL,
            PRIMARY KEY  id (id)
        ) DEFAULT CHARSET=utf8;";

    if ($this['db']->executeQuery($sql) === false) {
      throw new \RuntimeException('Unable to create Widgetkit database.');
    }
  }

  public function uninstall()
  {
    $sql = "DROP TABLE IF EXISTS @photoreact";

    if ($this['db']->executeQuery($sql) === false) {
      throw new \RuntimeException('Unable to delete Widgetkit database.');
    }
  }

  public static function getSubscribedEvents()
  {
    return array(
      'init'        => array('init', -5),
      'init.site'   => 'initSite',
      'init.admin'  => 'initAdmin',
      'view.render' => 'viewRender'
    );
  }
}
