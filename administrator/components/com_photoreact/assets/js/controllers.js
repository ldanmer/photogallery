!function () {
    var t = function (t, e, n, a, cf) {
        // init config

        t.config = t.data.widgets.calc;
        t.config.data = angular.extend({}, t.config.settings, t.data.calculator.data);

        var i = this,
            d = window.localStorage || {};
        i.viewmode = d["wk.content.viewmode"] || "list", i.include = "", i.previewContent = function (e) {
            return t.$emit("wk.preview.content", e).preview || t.data.types[e.type].icon
        }, i.createContent = function (e) {
            t.content = e || {
                name: "",
                type: "custom",
                data: {
                    _widget: {
                        name: 'grid'
                    },
                },
                date: new Date().toLocaleDateString('ru-RU'),
                photo: ''
            }, t.widget = null, i.setView("contentConfig")
        }, i.editContent = function (e, n) {
            let a, d = null, o = t.data.widgets;
            return e = angular.copy(e), a = e.data._widget, e.id || angular.extend(e.data, t.data.types[e.type].data), o[a.name] ? (d = angular.copy(o[a.name]), d.data = a.data = angular.extend({}, d.settings, a.data), t.content = e, t.widget = d, void i.setView("contentEdit", n)) : void i.createContent(e)
        }, i.saveContent = function () {
            return a.save({
                id: t.content.id
            }, {
                content: t.content
            }, function (e) {
                if (i.include !== 'widget')
                    i.editContent(t.data.content[e.id] = e);
                n.notify(e.name + " saved.", "success")
            })
        }, i.saveConfig = function () {
            return cf.save({
                config: t.config
            }, function (e) {
                n.notify(e.name + " saved.", "success");
            })
        }, i.copyContent = function (e) {
            return e = angular.copy(e), e.id = "", e.name += " (copy)", a.save({
                id: e.id
            }, {
                content: e
            }, function (e) {
                t.data.content[e.id] = e, e.data._widget = angular.isArray(e.data._widget) ? {} : e.data._widget, n.notify(e.name + " copied.", "success")
            })
        }, i.deleteContent = function (e) {
            confirm("Are you sure?") && a["delete"]({
                id: e.id
            }, function () {
                delete t.data.content[e.id]
            })
        }, i.getWidget = function (e) {
            return t.data.widgets[e.data._widget.name]
        }, i.selectWidget = function (e) {
            var n = t.content.data;
            n._widget.name != e.name && (n._widget.name = e.name, n._widget.data = {})

        }, i.setView = function (e, n, item = {}) {
            i.view = e, n && (i.include = n), t.$emit("wk.change.view", e);
            if (n === 'widget' && !!item) {
                t.widget.current = item.uid || 0;
            }
        }, i.setViewMode = function (t) {
            i.viewmode = d["wk.content.viewmode"] = t
        },
            i.setView("content")
    };
    angular.module("photoreact").controller("contentCtrl", ["$scope", "Application", "UIkit", "Content", "mediaPicker", "Config", function (e, n, a, i, o, cf) {
        var d = this;
        e.data = angular.extend({
            content: i.query(function (t) {
                angular.forEach(t, function (t, e) {
                    "$" !== e[0] && (t.data = angular.extend({
                        _widget: {}
                    }, t.data), t.data._widget = angular.isArray(t.data._widget) ? {} : t.data._widget)
                })
            })
        }, n.config), d.name = "contentCtrl", t.call(this, e, n, a, i, cf);
        d.selectMedia = function () {
            o.select().then(function (t) {
                e.content.photo = t.href;
            })
        };
        d.previewItem = function (c) {
            return c.photo || e.data.types.custom.icon;
        };
        d.addressComplete = function () {
            jQuery("#autocomplete").suggestions({
                token: "3907b83f23936eff1afbbc28849266750c68f3a5",
                type: "ADDRESS",
                constraints: [{
                    locations: {kladr_id: '69'},
                    deletable: true
                }],
                restrict_value: true
            });
        };

    }]).controller("pickerCtrl", ["$scope", "Application", "Content", "UIkit", function (e, n, a, i) {
        var d = this;
        e.data = angular.extend({}, n.config), e.data.content = a.query(function (t) {
            angular.forEach(t, function (t, e) {
                "$" !== e[0] && (t.data = angular.extend({
                    _widget: {}
                }, t.data), t.data._widget = angular.isArray(t.data._widget) ? {} : t.data._widget)
            });
            var a = e.data.content[n.env.attrs.id];
            a && "editor" === d.mode && (d.editContent(a, "content"), d.mode = "edit"), n.env.modal.show()
        }), d.name = "pickerCtrl", d.mode = n.env.mode, d.active = function (t) {
            return t.id === n.env.attrs.id
        }, d.update = function (t) {
            n.env.update({
                id: t.id,
                name: t.name
            })
        }, d.cancel = function () {
            n.env.cancel()
        }, t.call(this, e, n, i, a)
    }])
}();