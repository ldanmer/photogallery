angular.module("photoreact").directive("mediaPreview", ["mediaInfo", function (e) {
    function i(i) {
        var r = this;
        r.type = function () {
            return i.media = e(i.src), i.media.type
        }, r.cleanUrl = function (e) {
            return "string" == typeof e && (e = e.replace("autoplay=1", "autoplay=0")), e
        }
    }

    return {
        restrict: "E",
        scope: {src: "@"},
        controller: ["$scope", i],
        controllerAs: "vm",
        template: '<div ng-switch="vm.type()">\n' +
                    '    <img class="wk-preview-thumb" ng-switch-default ng-src="{{ media.src }}">\n' +
                    '</div>'
    }
}]).directive("autofocus", ["$timeout", function (e) {
    var i = [];
    return {
        restrict: "A", link: function (r, t) {
            i.push(t), e(function () {
                i[0][0].focus()
            })
        }
    }
}]);