angular.module("Fields", []).directive("fieldMedia", ["mediaPicker", "mediaInfo", function (e, t) {
    function i(i) {
        let a = this;
        a.selectMedia = function () {
            e.select({multiple: !0}).then(function (e) {
                if (angular.isArray(e)) {
                    if (typeof i.media === 'undefined') {
                        i.media = [];
                        console.log(i.media)
                    }
                    angular.forEach(e, (t) => {
                        i.media.push(t.url)
                    })
                }
            })
        };
        a.remove = (el) => {
            i.media = i.media.filter(img => img !== el);
        }
    }

    var n = {};
    return {
        scope: {
            media: "=",
            options: "=?",
            title: "=?"
        },
        restrict: "E",
        controller: ["$scope", i],
        controllerAs: "vm",
        template: '<div>\n' +
            '\t<div class="uk-flex">\n' +
            '      <button class="uk-button" ng-click="vm.selectMedia()">Ракурсы</button>\n' +
            '\t\t\t<div id="js-media-multiple" class="uk-flex" ng-show="media.length">\n' +
            '\t\t\t\t<div ng-repeat="i in media" ng-mouseover="removeMask = true" ng-mouseleave="removeMask = false" class="uk-margin-left" ng-click="vm.remove(i)">\n' +
            '\t\t\t\t<span ng-show="removeMask" class="removeMask"><i>&times;</i></span>\n' +
            '\t\t\t\t\t<input type="hidden" ng-model="i">\t\t\t\n' +
            '\t\t\t\t\t<media-preview src="{{ i }}"></media-preview>\n' +
            '\t\t\t\t</div>\n' +
            '\t\t\t</div>\n' +
            '   </div>\n' +
            '</div>'
    }
}]).directive("fieldLocation", ["$timeout", "$q", function (e, t) {
    var i = 0,
        n = function () {
            var e, i = function () {
                if (!e) {
                    e = t.defer();
                    var i = document.createElement("script");
                    i.async = !0, i.onload = function () {
                        google.load("maps", "3", {
                            other_params: "sensor=false&libraries=places",
                            callback: function () {
                                google && google.maps.places && e.resolve()
                            }
                        })
                    }, i.onerror = function () {
                        alert("Failed loading google maps api.")
                    }, i.src = "https://www.google.com/jsapi", document.getElementsByTagName("head")[0].appendChild(i)
                }
                return e.promise
            };
            return i
        }();
    return {
        restrict: "EA",
        require: "?ngModel",
        scope: {
            latlng: "@"
        },
        replace: !0,
        template: '<div>                                <div class="uk-form uk-form-icon uk-margin-small-bottom uk-width-1-1">                                    <i class="uk-icon-search"></i><input class="uk-width-1-1">                                </div>                                <div class="js-map" style="min-height:300px;">                                 Loading map...                                 </div>                                 <div class="uk-text-small uk-margin-small-top">LAT: <span class="uk-text-muted">{{ latlng.lat }}</span> LNG: <span class="uk-text-muted">{{ latlng.lng }}</span></div>                            </div>',
        link: function (t, a, o, l) {
            function s(e) {
                l.$setViewValue(e), t.latlng = e, t.$root.$$phase || t.$apply()
            }

            n().then(function () {
                e(function () {
                    var e, n, o, r, c = "wk-location-" + ++i,
                        u = new google.maps.LatLng(53.55909862554551, 9.998652343749995);
                    t.latlng = l.$viewValue || {
                        lat: u.lat(),
                        lng: u.lng()
                    }, a.find(".js-map").attr("id", c), e = new google.maps.Map(document.getElementById(c), {
                        zoom: 6,
                        center: u
                    }), n = new google.maps.Marker({
                        position: u,
                        map: e,
                        draggable: !0
                    }), google.maps.event.addListener(n, "dragend", function () {
                        var e = n.getPosition();
                        s({
                            lat: e.lat(),
                            lng: e.lng()
                        }), o.value = ""
                    }), jQuery.UIkit.$win.on("resize", function () {
                        google.maps.event.trigger(e, "resize"), e.setCenter(n.getPosition())
                    }), o = a.find("input")[0], r = new google.maps.places.Autocomplete(o), r.bindTo("bounds", e), google.maps.event.addListener(r, "place_changed", function () {
                        var t = r.getPlace();
                        if (t.geometry) {
                            t.geometry.viewport ? e.fitBounds(t.geometry.viewport) : e.setCenter(t.geometry.location), n.setPosition(t.geometry.location), o.value = "";
                            var i = n.getPosition();
                            s({
                                lat: i.lat(),
                                lng: i.lng()
                            })
                        }
                    }), google.maps.event.addDomListener(o, "keydown", function (e) {
                        13 == e.keyCode && e.preventDefault()
                    }), l.$render = function () {
                        try {
                            if (l.$viewValue && l.$viewValue.lat && l.$viewValue.lng) {
                                var t = new google.maps.LatLng(l.$viewValue.lat, l.$viewValue.lng);
                                n.setPosition(t), e.setCenter(t)
                            } else s({
                                lat: n.getPosition().lat(),
                                lng: n.getPosition().lng()
                            })
                        } catch (i) {
                        }
                    }, l.$render()
                })
            })
        }
    }
}]).factory("Fields", function () {
    var e = {
        text: {
            label: "Text",
            template: function (e, t) {
                var i = angular.element('<input class="uk-width-1-1" type="text"  ng-model="' + e + '">').attr(t.attributes || {});
                return t && t.icon && (i = i.wrap('<div class="uk-form-icon uk-width-1-1"></div>').before('<i class="uk-icon-' + t.icon + '"></i>').parent()), i
            }
        },
        textarea: {
            label: "Textarea",
            template: function (e, t) {
                return angular.element('<textarea id="wk-content" class="uk-width-1-1" ng-model="' + e + '" rows="10"></textarea>').attr(t.attributes || {})
            }
        },
        tags: {
            label: "Tags",
            template: function (e, t) {
                return angular.element('<div class="uk-form-icon uk-width-1-1"><i class="uk-icon-tags"></i><input class="uk-width-1-1" type="text" ng-list ng-model="' + e + '" placeholder="tag, tag, ..."></div><div>').find("input").attr(t.attributes || {}).parent()
            }
        },
        "boolean": {
            label: "Boolean",
            template: function (e, t) {
                return angular.element('<input type="checkbox" ng-model="' + e + '">').attr(t.attributes || {})
            }
        },
        media: {
            label: "Media",
            template: function (e) {
                return '<field-media media="' + e + '"></field-media>'
            }
        },
        location: {
            label: "Location",
            template: function (e) {
                return '<field-location  ng-model="' + e + '"></field-location>'
            }
        }
    };
    return {
        register: function (t, i) {
            e[t] = angular.extend({
                label: t,
                assets: [],
                template: function () {
                }
            }, i)
        },
        exists: function (t) {
            return e[t] ? !0 : !1
        },
        get: function (t) {
            return e[t]
        },
        fields: function () {
            var t = [];
            return Object.keys(e).forEach(function (i) {
                t.push({
                    name: i,
                    label: e[i].label
                })
            }), t
        }
    }
}).directive("field", ["$timeout", "$compile", "Fields", function (e, t, i) {
    return {
        require: "?ngModel",
        restrict: "E",
        link: function (n, a, o) {
            var l = function () {
                var e = angular.extend({}, JSON.parse(o.options || "{}")),
                    l = o.type || "text";
                if (i.exists(l)) {
                    var s, r = i.get(l);
                    s = r.template(o.ngModel, e), s.then ? s.then(function (e) {
                        t(a.html(e).contents())(n)
                    }) : t(a.html(s).contents())(n)
                } else t(a.html(i.get("text").template(o.ngModel)).contents())(n)
            };
            e(l)
        }
    }
}]);