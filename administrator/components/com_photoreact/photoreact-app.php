<?php

defined('_JEXEC') or die;

if ($component = JComponentHelper::getComponent('com_photoreact', true) and $component->enabled) {
    return include(__DIR__ . '/photoreact.php');
}

return false;