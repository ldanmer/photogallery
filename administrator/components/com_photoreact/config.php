<?php

return [
    'name'        => 'photoreact',
    'version'     => '1.0.0',
    'db_version'  => '2.0.0',
    'path'        => __DIR__,
    'path.cache'  => __DIR__.'/cache',
    'path.vendor' => __DIR__.'/vendor',
    'debug'       => 0
];
