<div class="uk-grid uk-grid-divider uk-form uk-form-horizontal" data-uk-grid-margin>
    <div class="uk-width-medium-1-3">
        <h3 class="wk-form-heading">{{'Материал' | trans}}</h3>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'МСД классик' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['classic']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'МСД премиум' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['premium']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'МСД премиум+' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['premium_plus']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'МСД премиум цветной' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['premium_color']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'МСД премиум фактурный' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['premium_factory']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Понгс 325' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['pongs']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Дескор (Д премиум)' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['descor']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Клипсо 495D' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['clipso']"></label>
            </div>
        </div>
    </div>
    <div class="uk-width-medium-1-3">
        <h3 class="wk-form-heading">{{'Работа' | trans}}</h3>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Маскировочная лента' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['mask_tape']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Установка люстры' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['chandler']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Установка светильника' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['lighter']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Обвод трубы' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['pipe_padding']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Установка потолочного карниза' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['ceiling_cornice']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Работа по плитке' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['tile_work']"></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Обработка углов' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="config.data['corner_work']"></label>
            </div>
        </div>

    </div>
</div>