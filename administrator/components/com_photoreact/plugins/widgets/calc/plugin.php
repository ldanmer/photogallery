<?php

return [

  'name' => 'widget/calc',

  'main' => 'Danmer\\Photoreact\\Widget\\Widget',

  'config' => [
    'name'     => 'calc',
    'label'    => 'Calc',
    'core'     => true,
    'icon'     => 'plugins/widgets/calc/widget.svg',
    'view'     => 'plugins/widgets/calc/views/widget.php',
    'settings' => [
      'classic'         => 300,
      'premium'         => 350,
      'premium_plus'    => 380,
      'premium_color'   => 380,
      'premium_factory' => 450,
      'pongs'           => 500,
      'descor'          => 900,
      'clipso'          => 1450,
      'mask_tape'       => 60,
      'chandler'        => 500,
      'lighter'         => 350,
      'pipe_padding'    => 300,
      'ceiling_cornice' => 1000,
      'tile_work'       => 100,
      'corner_work'     => 50,
    ]
  ],

  'events' => [
    'init.admin' => function ($event, $app) {
      $app['angular']->addTemplate('calc.edit', 'plugins/widgets/calc/views/edit.php', true);
    }
  ]
];
