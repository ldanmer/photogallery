<?php

return [

  'name' => 'widget/grid',

  'main' => 'Danmer\\Photoreact\\Widget\\Widget',

  'config' => [
    'name'  => 'grid',
    'label' => 'Grid',
    'core'  => true,
    'icon'  => 'plugins/widgets/grid/widget.svg',
    'view'  => 'plugins/widgets/grid/views/widget.php',
    'item'  => ['title', 'content', 'media'],
    'settings' => []
  ],

  'events' => [

        'init.site' => function ($event, $app) {
            $app['scripts']->add('uikit-grid', 'vendor/assets/uikit/js/components/grid.min.js', ['uikit']);
        },

        'init.admin' => function ($event, $app) {
            $app['angular']->addTemplate('grid.edit', 'plugins/widgets/grid/views/edit.php', true);
        }

  ]

];
