<div class="uk-grid uk-grid-divider uk-form uk-form-horizontal" data-uk-grid-margin>
    <div class="uk-width-medium-1-3">
        <h3 class="wk-form-heading">{{'База' | trans}}</h3>
        <div class="uk-form-row">
            <label class="uk-form-label" for="wk-grid">{{'Материал' | trans}}</label>
            <div class="uk-form-controls">
                <select id="wk-grid" class="uk-form-width-medium" ng-model="widget.data[widget.current]['material']" ng-init="widget.data[widget.current]['material'] = 'classic'">
                    <option value="classic">{{'МСД классик' | trans}}</option>
                    <option value="premium">{{'МСД премиум' | trans}}</option>
                    <option value="premium_plus">{{'МСД премиум+' | trans}}</option>
                    <option value="premium_color">{{'МСД премиум цветной' | trans}}</option>
                    <option value="premium_factory">{{'МСД премиум фактурный' | trans}}</option>
                    <option value="pongs">{{'Понгс 325' | trans}}</option>
                    <option value="descor">{{'Дескор (Д премиум)' | trans}}</option>
                    <option value="clipso">{{'Клипсо 495D' | trans}}</option>
                </select>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Площадь' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['square']" ng-init="widget.data[widget.current]['square'] = 1"> м<sup>2</sup></label>
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Маскировочная лента' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['mask_tape']['value']" ng-init="widget.data[widget.current]['mask_tape']['value'] = 0"> м.п.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['mask_tape']['name']" ng-value="widget.data[widget.current]['mask_tape']['name'] = 'Маскировочная лента'">
                <input type="hidden" ng-model="widget.data[widget.current]['mask_tape']['unit']" ng-value="widget.data[widget.current]['mask_tape']['unit'] = 'м.п.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Установка люстры' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['chandler']['value']" ng-init="widget.data[widget.current]['chandler']['value'] = 0"> шт.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['chandler']['name']" ng-value="widget.data[widget.current]['chandler']['name'] = 'Установка люстры'">
                <input type="hidden" ng-model="widget.data[widget.current]['chandler']['unit']" ng-value="widget.data[widget.current]['chandler']['unit'] = 'шт.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Установка светильника' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['lighter']['value']" ng-init="widget.data[widget.current]['lighter']['value'] = 0"> шт.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['lighter']['name']" ng-value="widget.data[widget.current]['lighter']['name'] = 'Установка светильника'">
                <input type="hidden" ng-model="widget.data[widget.current]['lighter']['unit']" ng-value="widget.data[widget.current]['lighter']['unit'] = 'шт.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Обвод трубы' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['pipe_padding']['value']" ng-init="widget.data[widget.current]['pipe_padding']['value'] = 0"> шт.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['pipe_padding']['name']" ng-value="widget.data[widget.current]['pipe_padding']['name'] = 'Обвод трубы'">
                <input type="hidden" ng-model="widget.data[widget.current]['pipe_padding']['unit']" ng-value="widget.data[widget.current]['pipe_padding']['unit'] = 'шт.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Установка потолочного карниза' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['ceiling_cornice']['value']" ng-init="widget.data[widget.current]['ceiling_cornice']['value'] = 0"> шт.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['ceiling_cornice']['name']" ng-value="widget.data[widget.current]['ceiling_cornice']['name'] = 'Установка потолочного карниза'">
                <input type="hidden" ng-model="widget.data[widget.current]['ceiling_cornice']['unit']" ng-value="widget.data[widget.current]['ceiling_cornice']['unit'] = 'шт.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Работа по плитке' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['tile_work']['value']" ng-init="widget.data[widget.current]['tile_work']['value'] = 0"> м.п.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['tile_work']['name']" ng-value="widget.data[widget.current]['tile_work']['name'] = 'Работа по плитке'">
                <input type="hidden" ng-model="widget.data[widget.current]['tile_work']['unit']" ng-value="widget.data[widget.current]['tile_work']['unit'] = 'м.п.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Обработка углов' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['corner_work']['value']" ng-init="widget.data[widget.current]['corner_work']['value'] = 0"> шт.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['corner_work']['name']" ng-value="widget.data[widget.current]['corner_work']['name'] = 'Обработка углов'">
                <input type="hidden" ng-model="widget.data[widget.current]['corner_work']['unit']" ng-value="widget.data[widget.current]['corner_work']['unit'] = 'шт.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Доп. работы' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['additional']['value']" ng-init="widget.data[widget.current]['additional']['value'] = 0"> &#8381;</label>
                <input type="hidden" ng-model="widget.data[widget.current]['additional']['name']" ng-value="widget.data[widget.current]['additional']['name'] = 'Доп. работы'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Итого' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" readonly ng-init="widget.data[widget.current]['sum'] = 0" ng-model="widget.data[widget.current]['sum']"> &#8381;</label>
            </div>
        </div>
    </div>
    <div class="uk-width-medium-1-3">
        <h3 class="wk-form-heading">{{'Конструкции' | trans}}</h3>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Установка закладной под шкаф-купе 40*60' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['closet']['value']"> м.п.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['closet']['name']" ng-value="widget.data[widget.current]['closet']['name'] = 'Установка закладной под шкаф-купе 40*60'">
                <input type="hidden" ng-model="widget.data[widget.current]['closet']['unit']" ng-value="widget.data[widget.current]['closet']['unit'] = 'м.п.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Брус 40*40' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['timber']['value']"> м.п.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['timber']['name']" ng-value="widget.data[widget.current]['timber']['name'] = 'Брус 40*40'">
                <input type="hidden" ng-model="widget.data[widget.current]['timber']['unit']" ng-value="widget.data[widget.current]['timber']['unit'] = 'м.п.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Ниша для скрытого карниза через AI брус' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['niche']['value']"> м.п.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['niche']['name']" ng-value="widget.data[widget.current]['niche']['name'] = 'Ниша для скрытого карниза через AI брус'">
                <input type="hidden" ng-model="widget.data[widget.current]['niche']['unit']" ng-value="widget.data[widget.current]['niche']['unit'] = 'м.п.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Бесщелевый уровневый потолок' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['gapless_ceiling']['value']"> м.п.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['gapless_ceiling']['name']" ng-value="widget.data[widget.current]['gapless_ceiling']['name'] = 'Бесщелевый уровневый потолок'">
                <input type="hidden" ng-model="widget.data[widget.current]['gapless_ceiling']['unit']" ng-value="widget.data[widget.current]['gapless_ceiling']['unit'] = 'м.п.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Уровневый потолок с подстветкой' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['illuminated_ceiling']['value']"> м.п.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['illuminated_ceiling']['name']" ng-value="widget.data[widget.current]['illuminated_ceiling']['name'] = 'Уровневый потолок с подстветкой'">
                <input type="hidden" ng-model="widget.data[widget.current]['illuminated_ceiling']['unit']" ng-value="widget.data[widget.current]['illuminated_ceiling']['unit'] = 'м.п.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Парящий потолок' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['soaring_ceiling']['value']"> м.п.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['soaring_ceiling']['name']" ng-value="widget.data[widget.current]['soaring_ceiling']['name'] = 'Парящий потолок'">
                <input type="hidden" ng-model="widget.data[widget.current]['soaring_ceiling']['unit']" ng-value="widget.data[widget.current]['soaring_ceiling']['unit'] = 'м.п.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Ниша со скрытым карнизом КП 05' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['niche_hidden_cornice']['value']"> м.п.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['niche_hidden_cornice']['name']" ng-value="widget.data[widget.current]['niche_hidden_cornice']['name'] = 'Ниша со скрытым карнизом КП 05'">
                <input type="hidden" ng-model="widget.data[widget.current]['niche_hidden_cornice']['unit']" ng-value="widget.data[widget.current]['niche_hidden_cornice']['unit'] = 'м.п.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Разделительный багет' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['dividing_baguette']['value']"> м.п.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['dividing_baguette']['name']" ng-value="widget.data[widget.current]['dividing_baguette']['name'] = 'Разделительный багет'">
                <input type="hidden" ng-model="widget.data[widget.current]['dividing_baguette']['unit']" ng-value="widget.data[widget.current]['dividing_baguette']['unit'] = 'м.п.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Отбойник' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['chipper']['value']"> м.п.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['chipper']['name']" ng-value="widget.data[widget.current]['chipper']['name'] = 'Отбойник'">
                <input type="hidden" ng-model="widget.data[widget.current]['chipper']['unit']" ng-value="widget.data[widget.current]['chipper']['unit'] = 'м.п.'">
            </div>
        </div>
        <div class="uk-form-row">
            <input class="uk-input uk-width-1-3" type="text" placeholder="Название" ng-model="widget.data[widget.current]['first']['name']">
            <input class="uk-input uk-width-1-5" type="text" placeholder="25" ng-model="widget.data[widget.current]['first']['value']">
            <select class="uk-width-1-5" ng-model="widget.data[widget.current]['first']['unit']">
                <option value="шт.">шт.</option>
                <option value="м2">м<sup>2</sup></option>
                <option value="м.п.">м.п.</option>
            </select>
        </div>
        <div class="uk-form-row">
            <input class="uk-input uk-width-1-3" type="text" placeholder="Название" ng-model="widget.data[widget.current]['second']['name']">
            <input class="uk-input uk-width-1-5" type="text" placeholder="25" ng-model="widget.data[widget.current]['second']['value']">
            <select class="uk-width-1-5" ng-model="widget.data[widget.current]['second']['unit']">
                <option value="шт.">шт.</option>
                <option value="м2">м<sup>2</sup></option>
                <option value="м.п.">м.п.</option>
            </select>
        </div>
    </div>
    <div class="uk-width-medium-1-3">
        <h3 class="wk-form-heading">{{'Изделия' | trans}}</h3>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Светильник GX53' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['lamp_gx53']['value']"> шт.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['lamp_gx53']['name']" ng-value="widget.data[widget.current]['lamp_gx53']['name'] = 'Светильник GX53'">
                <input type="hidden" ng-model="widget.data[widget.current]['lamp_gx53']['unit']" ng-value="widget.data[widget.current]['lamp_gx53']['unit'] = 'шт.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Лампа светодиодная GX53' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['lamp_diode_gx53']['value']"> шт.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['lamp_diode_gx53']['name']" ng-value="widget.data[widget.current]['lamp_diode_gx53']['name'] = 'Лампа светодиодная GX53'">
                <input type="hidden" ng-model="widget.data[widget.current]['lamp_diode_gx53']['unit']" ng-value="widget.data[widget.current]['lamp_diode_gx53']['unit'] = 'шт.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Светодиодная лента PREMIUM' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['lamp_diode_premium']['value']"> шт.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['lamp_diode_premium']['name']" ng-value="widget.data[widget.current]['lamp_diode_premium']['name'] = 'Светодиодная лента PREMIUM'">
                <input type="hidden" ng-model="widget.data[widget.current]['lamp_diode_premium']['unit']" ng-value="widget.data[widget.current]['lamp_diode_premium']['unit'] = 'шт.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Блок питания 12/24v' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['power_supply']['value']"> шт.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['power_supply']['name']" ng-value="widget.data[widget.current]['power_supply']['name'] = 'Блок питания 12/24v'">
                <input type="hidden" ng-model="widget.data[widget.current]['power_supply']['unit']" ng-value="widget.data[widget.current]['power_supply']['unit'] = 'шт.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Контроллер +ПДУ для ленты' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['pdu_controller_tape']['value']"> шт.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['pdu_controller_tape']['name']" ng-value="widget.data[widget.current]['pdu_controller_tape']['name'] = 'Контроллер +ПДУ для ленты'">
                <input type="hidden" ng-model="widget.data[widget.current]['pdu_controller_tape']['unit']" ng-value="widget.data[widget.current]['pdu_controller_tape']['unit'] = 'шт.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Контроллер +ПДУ для светильников' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['pdu_controller_lamp']['value']"> шт.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['pdu_controller_lamp']['name']" ng-value="widget.data[widget.current]['pdu_controller_lamp']['name'] = 'Контроллер +ПДУ для светильников'">
                <input type="hidden" ng-model="widget.data[widget.current]['pdu_controller_lamp']['unit']" ng-value="widget.data[widget.current]['pdu_controller_lamp']['unit'] = 'шт.'">
            </div>
        </div>
        <div class="uk-form-row">
            <label class="uk-form-label">{{'Карниз потолочный с блендой' | trans}}</label>
            <div class="uk-form-controls">
                <label><input class="uk-form-width-small" type="text" ng-model="widget.data[widget.current]['cornice_hood']['value']"> шт.</label>
                <input type="hidden" ng-model="widget.data[widget.current]['cornice_hood']['name']" ng-value="widget.data[widget.current]['cornice_hood']['name'] = 'Карниз потолочный с блендой'">
                <input type="hidden" ng-model="widget.data[widget.current]['cornice_hood']['unit']" ng-value="widget.data[widget.current]['cornice_hood']['unit'] = 'шт.'">
            </div>
        </div>
        <div class="uk-form-row">
            <input class="uk-input uk-width-1-3" type="text" placeholder="Название" ng-model="widget.data[widget.current]['third']['name']">
            <input class="uk-input uk-width-1-5" type="text" placeholder="25" ng-model="widget.data[widget.current]['third']['value']">
            <select class="uk-width-1-5" ng-model="widget.data[widget.current]['third']['unit']">
                <option value="шт.">шт.</option>
                <option value="м2">м<sup>2</sup></option>
                <option value="м.п.">м.п.</option>
            </select>
        </div>
        <div class="uk-form-row">
            <input class="uk-input uk-width-1-3" type="text" placeholder="Название" ng-model="widget.data[widget.current]['forth']['name']">
            <input class="uk-input uk-width-1-5" type="text" placeholder="25" ng-model="widget.data[widget.current]['forth']['value']">
            <select class="uk-width-1-5" ng-model="widget.data[widget.current]['forth']['unit']">
                <option value="шт.">шт.</option>
                <option value="м2">м<sup>2</sup></option>
                <option value="м.п.">м.п.</option>
            </select>
        </div>
        <div class="uk-form-row">
            <input class="uk-input uk-width-1-3" type="text" placeholder="Название" ng-model="widget.data[widget.current]['fifth']['name']">
            <input class="uk-input uk-width-1-5" type="text" placeholder="25" ng-model="widget.data[widget.current]['fifth']['value']">
            <select class="uk-width-1-5" ng-model="widget.data[widget.current]['fifth']['unit']">
                <option value="шт.">шт.</option>
                <option value="м2">м<sup>2</sup></option>
                <option value="м.п.">м.п.</option>
            </select>
        </div>
        <div class="uk-form-row">
            <input class="uk-input uk-width-1-3" type="text" placeholder="Название" ng-model="widget.data[widget.current]['sixth']['name']">
            <input class="uk-input uk-width-1-5" type="text" placeholder="25" ng-model="widget.data[widget.current]['sixth']['value']">
            <select class="uk-width-1-5" ng-model="widget.data[widget.current]['sixth']['unit']">
                <option value="шт.">шт.</option>
                <option value="м2">м<sup>2</sup></option>
                <option value="м.п.">м.п.</option>
            </select>
        </div>
    </div>
</div>