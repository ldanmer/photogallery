<?php

return array(

  'name' => 'content/custom',

  'main' => 'Danmer\\Photoreact\\Content\\Type',

  'config' => [
    'name'  => 'custom',
    'label' => 'Custom',
    'icon'  => 'assets/images/content-placeholder.svg',
    'item'  => ['title', 'content', 'media'],
    'fields' => [
      'email'       => ['type' => 'text', 'label' => 'Email', 'options' => ['icon' => 'envelope-o', 'attributes' => ['placeholder' => 'your@email.com']]],
      'location'    => ['type' => 'location', 'label' => 'Location'],
      'tags'        => ['type' => 'tags', 'label' => 'Tags'],
    ],
    'data'  => [
            'items' => []
    ]

  ],

  'items' => function ($items, $content) {

        if (is_array($content['items'])) {
            foreach ($content['items'] as $data) {
                $items->add($data);
            }
        }
    },

  'events' => array(

        'init.admin' => function ($event, $app) {
            $app['scripts']->add('photoreact-custom-controller', 'plugins/content/custom/assets/controller.js');
            $app['angular']->addTemplate('custom.edit', 'plugins/content/custom/views/edit.php', true);
        }

    )

);
