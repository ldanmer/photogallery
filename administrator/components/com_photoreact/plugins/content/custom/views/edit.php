<div ng-controller="customCtrl as custom">

    <div class="uk-grid uk-grid-divider uk-form uk-form-stacked" data-uk-grid-margin>
        <div ng-class="vm.name == 'contentCtrl' ? 'wk-width-xlarge-1-4' : ''" class="uk-width-medium-1-3">

            <div class="wk-panel-marginless">
                <ul id="js-content-items" class="uk-nav uk-nav-side" data-uk-sortable="{dragCustomClass:'wk-sortable'}"
                    ng-show="content.data.items.length">
                    <li class="uk-visible-hover" ng-repeat="item in content.data.items"
                        ng-class="(item === $parent.item ? 'uk-active':'')">
                        <div class="wk-subnav-right uk-hidden">
                            <ol class="uk-subnav wk-subnav-icon">
                                <li>
                                    <a ng-click="custom.deleteItem(item)" ng-class="js-delete-room"><i class="uk-icon-times"></i></a>
                                </li>
                            </ol>
                        </div>
                        <a ng-click="custom.editItem(item)">
                            <div class="wk-preview-thumb uk-cover-background uk-margin-small-right"
                                 ng-style="{'background-image': 'url(' + custom.previewItem(item) + ')'}"></div>
                            {{ item.title }}
                        </a>
                    </li>
                </ul>

                <p class="uk-margin">
                    <button class="uk-button" ng-click="custom.addItem()">{{'Добавить помещение' | trans}}</button>
                </p>
            </div>
        </div>
        <div ng-class="vm.name == 'contentCtrl' ? 'wk-width-xlarge-3-4' : ''" class="uk-width-medium-2-3"
             ng-show="item">
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <select class="uk-form-large uk-width-1-1 uk-margin-bottom" id="wk-room" ng-model="item.room"
                            ng-options="room for room in data.rooms | toArray" autofocus>
                        <option value="">- {{'Выберите помещение'}} -</option>
                    </select>

                    <select class="uk-form-large uk-width-1-1 uk-margin-bottom" id="wk-texture" ng-model="item.texture"
                            ng-options="texture for texture in data.texture | toArray">
                        <option value="">- {{'Выберите фактуру'}} -</option>
                    </select>

                    <select class="uk-form-large uk-width-1-1 uk-margin-bottom" id="wk-feature" ng-model="item.feature"
                            ng-options="feature for feature in data.feature | toArray">
                        <option value="">- {{'Выберите особенность'}} -</option>
                    </select>
                    <button class="uk-button" ng-click="custom.mainPhoto(item)">{{'Главное фото' | trans}}</button>
                </div>
            </div>
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <field-media title="content.name" media="item.fores"></field-media>
                </div>
            </div>
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    <button class="uk-button" ng-click="vm.setView('contentEdit', 'widget', item)" class="ng-binding">{{'Смета' | trans}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
