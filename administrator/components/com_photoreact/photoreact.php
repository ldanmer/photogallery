<?php

use Danmer\Photoreact\Application;

global $photoreact;

if ($photoreact) {
  return $photoreact;
}

$loader = require __DIR__ . '/vendor/autoload.php';
$config = require __DIR__ . '/config.php';

$app = new Application($config);
$app['autoloader'] = $loader;
$app['path.cache'] = rtrim(JPATH_ROOT, '/') . '/cache/photoreact';
$app['component'] = 'com_' . $app['name'];
$app['permissions'] = array('core.manage' => 'manage_photoreact');
$app['templates'] = function () {
  return glob(rtrim(JPATH_ROOT, '/') . '/templates/*/photoreact') ?: array();
};

$app->on('init', function ($event, $app) {
  if ($app['admin'] && $app['component'] == JAdministratorHelper::findOption()) {
    $app->trigger('init.admin', array($app));
  }
});

$app->on('init.admin', function ($event, $app) {
  $document = JFactory::getDocument();

  JHtmlBehavior::keepalive();
  JHtml::_('jquery.framework');

  // don't check for component when installing
  if ($app['request']->get('option') != 'com_installer') {
    $app['config']->add(JComponentHelper::getParams($app['component'])->toArray());
  }

  $app['angular']->addTemplate('media', 'views/media.php', true);
  $app['angular']->set('token', JSession::getFormToken());
  $document->addScript('https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/js/jquery.suggestions.min.js');
  $document->addStyleSheet('https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/css/suggestions.min.css');
  $document->addStyleSheet('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
  $app['scripts']->add('photoreact-joomla', 'assets/js/joomla.js', ['photoreact-application']);
  $app['scripts']->add('photoreact-joomla-media', 'assets/js/joomla.media.js', ['photoreact-joomla']);
  $app['scripts']->add('uikit-upload');

  $app['styles']->add('photoreact-joomla', 'assets/css/joomla.css');
}, 10);

$app->on('view', function ($event, $app) {
  $app['config']->set('theme.support', $app['joomla.config']->get('photoreact'));
});

$app->boot();


return $photoreact = $app;
