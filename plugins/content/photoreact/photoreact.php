<?php

defined('_JEXEC') or die;

class plgContentPhotoreact extends JPlugin
{
  private $componentPath = JPATH_SITE . '/components/com_photoreact/';

  public function onContentPrepare($context, &$article, &$params, $limitstart = 0)
  {
    if (!$app = @include(JPATH_ADMINISTRATOR . '/components/com_photoreact/photoreact-app.php')) {
      return false;
    }

    $article->text = $app['shortcode']->parse('photoreact', $article->text, function ($attrs) use ($app) {
      return $this->renderWidget($attrs);
    });

  }

  public function renderWidget(array $attrs): string
  {
    require_once($this->componentPath . 'photoreact-app.php');
    if (!empty($attrs['room']) || !empty($attrs['texture']) || !empty($attrs['feature'])) {
      JLoader::register('PhotoreactModelAddress', $this->componentPath . 'models/address.php');
      JLoader::register('PhotoreactControllerAddress', $this->componentPath . 'controllers/address.php');
      $model = new PhotoreactModelAddress();
      $controller = new PhotoreactControllerAddress();
      $items = $model->getItems();

      $layout = new JLayoutFile('list', $this->componentPath . '/layouts');

      return $layout->render([
        'items' => $items,
        'material' => $controller->material,
        'room' => mb_strtolower($attrs['room']),
        'texture' => mb_strtolower($attrs['texture']),
        'feature' => mb_strtolower($attrs['feature']),
      ]);

    }
    return '';
  }
}
