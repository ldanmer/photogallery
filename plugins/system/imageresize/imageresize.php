<?php

defined('_JEXEC') or die;
require_once 'ImageResizeClass.php';
require_once 'watermark.php';

class PlgSystemImageresize extends JPlugin
{
  const WATER_MARK = JPATH_ROOT . '/media/com_photoreact/watermark.png';

  public function onContentAfterSave($context, $item, $isNew, $data = [])
  {
    if ($context == 'com_media.file' && !empty($item->filepath)) {
      $imageInfo = pathinfo($item->filepath);
      $thumbDir = $imageInfo['dirname'] . '/thumb/';
      if (!file_exists($thumbDir)) {
        mkdir($thumbDir, 0755, true);
      }
      try {
        $image = new ImageResize($item->filepath);
        $image->resizeToBestFit(800, 600)
          ->save($item->filepath)
          ->resizeToWidth(300)
          ->crop(300, 300)
          ->save($thumbDir . $imageInfo['basename']);

        $water = new Watermark();
        $water->apply($item->filepath, $item->filepath, self::WATER_MARK, 0);
      } catch (Exception $e) {

      }
    }
  }
}
