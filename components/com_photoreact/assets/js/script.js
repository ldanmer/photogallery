window.jQuery(document).ready(function ($) {
    $(document).on('click', '.js-photoreact--room', function () {
        const $this = $(this);
        const $detail = $this.next('div.photoreact--detail');
        const $container = $this.parents('div.js-photoreact--container');
        const isActive = $this.hasClass('active');
        closeDetail();
        if (!isActive) {
            $this.addClass('active');
            $detail.removeClass('uk-hidden');
            setTimeout(() => $container.css('margin-bottom', `${parseInt($detail.height(), 10) + 80}px`), 100);
            if (window.innerWidth < 1024) {
                location.hash = $('.js-photoreact--room').index($this);
            }
        }
    });
    $(document).on('click', '.js-photoreact--detail_close', function (e) {
        e.preventDefault();
        closeDetail();
    });

    window.addEventListener("popstate", function (e) {
        if (window.innerWidth < 1024 && !location.hash) {
            closeDetail();
        }
    });


    $(document).on('click', '[uk-slideshow-item]', function (e) {
        e.preventDefault();
        $('[uk-slideshow-item]').removeClass('uk-active');
        const index = $(this).parent('ul').find('[uk-slideshow-item]').index(this);
        const $parentWrapper = $(this).closest('div.photoreact--detail');
        const $slides = $parentWrapper.find('ul.uk-slideshow-items > li');
        $slides.removeClass('uk-active');
        $slides.eq(index).addClass('uk-active');
    });

    const closeDetail = () => {
        if (window.innerWidth < 1024) {
            const id = location.hash;
            if (!!id) {
                const matches = id.match(/(\d+)/);
                location.hash = '';
                $("body,html").animate({
                        scrollTop: $('.js-photoreact--room').eq(matches[0]).offset().top - 20
                    },
                    0
                );
            }
        }
        $('.js-photoreact--room').removeClass('active');
        $('.photoreact--detail').addClass('uk-hidden');
        $('.uk-transition-active').removeClass('uk-transition-active');
        $('div.js-photoreact--container').removeAttr('style');
    };

    $(document).on('click', '.js-ceiling-filter_form label', (e) => {
        e.preventDefault();
        const $this = $(e.target);
        const $form = $this.parents('form.js-ceiling-filter_form');
        $('.js-ceiling-filter_form label').removeClass('active');
        $this.addClass('active');
        $('input', $form).attr('checked', false);
        $this.find('input').attr('checked', true);
        $form.trigger('change');
    });

    $(document).on('change', '.js-ceiling-filter_form', (e) => {
        const $form = $(e.target).find('input:checked')[0];
        const val = $form.value;
        const name = $form.name;
        const label = $form.parentElement.innerText.trim();
        const dataContainer = '.js-photoreact--data';
        const $dataContainer = $(dataContainer);
        const photoContainer = '.js-photoreact--container';
        $dataContainer.show();
        $(photoContainer).prev('p').show();

        $('.js-filter--label').text(label);

        if (name !== 'all' && val !== "*") {
            $dataContainer.not(`[data-${name}="${val}"]`).hide();
            $(photoContainer).each(function () {
                const thisContainer = $(this);
                if (!thisContainer.find(`${dataContainer}:visible`).length) {
                    thisContainer.prev('p').hide();
                }
            });
        }
        $('.uk-modal-close-default').trigger('click');
    });

    const win = $(window);
    let counter = 0;
    let loading = false;
    win.scroll(function () {
        if (((win.scrollTop() + win.height()) + 50) >= $(document).height()) {
            if (loading === false) {
                loading = true;
                counter = counter + 5;
                const url = 'index.php?option=com_photoreact&task=address.loadOffset&offset=' + counter;
                $.ajax({
                    url: url,
                    dataType: 'html',
                    success: function (data) {
                        $('#gallery-loader').append(data);
                        loading = false;
                    }
                });
            }
        }
    });

    $('.js-photoreact-item').each((_, el) => {
        const $el = $(el);
        if ($el.find('.js-photoreact--data').length === 0) {
            $el.find('.js-photoreact--title').hide();
        }
    });
});

