<?php defined('JPATH_BASE') or die; ?>
<?php

use Danmer\Photoreact\Helper\Calculator;

$calculator = new Calculator('calculator.json');
try {
    $this->filter = $calculator->getData();
} catch (JsonException $e) {
    echo $e->getMessage();
}
$config = get_object_vars($this->filter->data);
/** @var ArrayObject $displayData */
$items = $displayData['items'];
$material = $displayData['material'];
$dataRoom = $displayData['room'];
$dataTexture = $displayData['texture'];
$dataFeature = $displayData['feature'];
?>
<div class="photoreact--list photoreact-wrapper">
    <? foreach ($items as $item): ?>
        <div class="js-photoreact-item">
            <p class="uk-text-bold uk-text-small mt-50 js-photoreact--title">
                <span class="uk-margin-small-right"><?= $item->date ?></span> <?= $item->name ?>
            </p>
            <? $data = json_decode($item->data, true) ?>
            <? if (is_array($data['items']) && count($data['items']) > 0): ?>
                <div class="uk-grid-small uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-5@m uk-grid-match uk-position-relative js-photoreact--container"
                     uk-grid>
                    <? foreach ($data['items'] as $room): ?>
                        <?
                        $sum = 0;
                        $roomInfo = $data['_widget']['data'][$room['uid']];
                        $materialSum = $config[$roomInfo['material']] * ($roomInfo['square'] * 1);

                        if (!empty($dataRoom) && $dataRoom != mb_strtolower($room['room'])) {
                            continue;
                        }

                        if (!empty($dataTexture) && $dataTexture != mb_strtolower($room['texture'])) {
                            continue;
                        }

                        if (!empty($dataFeature) && $dataFeature != mb_strtolower($room['feature'])) {
                            continue;
                        }
                        ?>
                        <div class="js-photoreact--data"
                             data-room="<?= mb_strtolower($room['room']) ?>"
                             data-texture="<?= mb_strtolower($room['texture']) ?>"
                             data-feature="<?= mb_strtolower($room['feature']) ?>"
                        >
                            <div class="js-photoreact--room">
                                <div class="uk-card uk-card-default">
                                    <?
                                    $imageInfo = pathinfo($room['media']);
                                    $thumb = "/{$imageInfo['dirname']}/thumb/{$imageInfo['basename']}";
                                    ?>
                                    <span class="img-wrap" style="background-image: url('<?= $thumb ?>')"></span>
                                    <div class="d-flex">
                                        <span><?= $room['room'] ?></span>
                                        <span class="uk-text-small uk-text-muted">
                                  <?= $roomInfo['square'] * 1 ?> м<sup>2</sup><br>
                                  <?= $materialSum * 1 ?> ₽
                                </span>
                                    </div>
                                </div>
                            </div>
                            <!--Detail-->
                            <div class="photoreact--detail uk-hidden">
                                <div class="uk-child-width-1-2@m" uk-grid>
                                    <div>
                                        <div class="uk-position-relative"
                                             uk-slideshow="animation: slide; draggable: false">
                                            <ul class="uk-slideshow-items">
                                                <li><img src="<?= $room['media'] ?>" alt=""></li>
                                                <? foreach ($room['fores'] as $img): ?>
                                                    <li><img src="<?= $img ?>" alt=""></li>
                                                <? endforeach; ?>
                                            </ul>
                                        </div>
                                        <div class="uk-position-relative uk-margin-top" uk-slider="finite: false">
                                            <ul class="uk-slider-items uk-thumbnav uk-grid-small uk-grid-match uk-child-width-1-5"
                                                uk-grid>
                                                <?
                                                $imageInfo = pathinfo($room['media']);
                                                $thumb = "/{$imageInfo['dirname']}/thumb/{$imageInfo['basename']}";
                                                ?>
                                                <li uk-slideshow-item="0" class="uk-active">
                                                    <a href="#">
                                                        <img src="<?= $thumb ?>" alt="">
                                                    </a>
                                                </li>
                                                <? foreach ($room['fores'] as $index => $img): ?>
                                                    <?
                                                    $imageInfo = pathinfo($img);
                                                    $thumb = "/{$imageInfo['dirname']}/thumb/{$imageInfo['basename']}";
                                                    ?>
                                                    <li uk-slideshow-item="<?= $index + 1 ?>">
                                                        <a href="#">
                                                            <img src="<?= $thumb ?>" alt="">
                                                        </a>
                                                    </li>
                                                <? endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="photoreact--detail_list">
                                        <a href="javascript:void;" class="js-photoreact--detail_close uk-align-right">&times;</a>
                                        <div class="uk-h1"><?= $room['room'] ?></div>
                                        <div class="uk-flex uk-text-small">
                                        <span class="uk-width-2-5 d-border-bottom">Материал
                                            <span class="js-photoreact--material"><?= $material[$roomInfo['material']] ?></span>
                                        </span>
                                            <span class="uk-width-1-5 d-border-bottom">
                                          <span class="js-photoreact--square"><?= $roomInfo['square'] * 1 ?></span> м<sup>2</sup>
                                      </span>
                                            <span class="uk-width-1-5 d-border-bottom uk-text-right">
                                          <span class="js-photoreact--square-sum"><?= $materialSum ?></span> ₽
                                      </span>
                                        </div>
                                        <?
                                        $i = 0;

                                        $filteredRoomInfo = [];
                                        $sum += $materialSum;
                                        foreach ($roomInfo as $index => $info) {
                                            if (empty($info['value']) || in_array($index, ['sum', 'square', 'material'])) continue;
                                            $filteredRoomInfo[$index] = $info;
                                            $sum += $config[$index] * ($info['value'] * 1);
                                        }
                                        $middle = (int)floor(count($filteredRoomInfo) / 2) - 1;
                                        ?>
                                        <? foreach ($filteredRoomInfo as $index => $info): ?>
                                            <div class="uk-flex uk-text-small">
                                                <span class="uk-width-2-5 d-border-bottom"><?= $info['name'] ?></span>
                                                <span class="uk-width-1-5 d-border-bottom"><?= $info['value'] ?> <?= $info['unit'] == 'м2' ? 'м<sup>2</sup>' : $info['unit'] ?></span>
                                                <span class="uk-width-1-5 uk-text-right">
                                                <? if ($i === $middle): ?>
                                                    <span class="js-photoreact--sum"><?= $sum - $materialSum ?></span> ₽
                                                <? endif ?>
                                                </span>
                                            </div>
                                            <? $i++ ?>
                                        <? endforeach; ?>
                                        <div class="uk-flex uk-text-small">
                                            <span class="uk-width-3-5">Итого: </span>
                                            <span class="uk-width-1-5 uk-text-right">
                                          <span class="js-photoreact--sum"><?= $sum ?></span> ₽
                                      </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--//Detail-->
                        </div>
                    <? endforeach; ?>
                </div>
            <? endif ?>
        </div>
    <? endforeach; ?>
</div>
