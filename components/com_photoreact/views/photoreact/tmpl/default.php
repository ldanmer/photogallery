<?php defined('_JEXEC') or die('Restricted access') ?>
<div class="photoreact-wrapper">
    <div class="uk-visible@m">
        <form class="js-ceiling-filter_form ceiling-filter_form-desktop">
            <div uk-grid>
                <label class="active"><input class="uk-radio" type="radio" name="all" value="*" checked> Все</label>
                <label for=""> | </label>
                <label><input class="uk-radio" type="radio" name="room" value="гостиная"> Гостиная</label>
                <label><input class="uk-radio" type="radio" name="room" value="спальня"> Спальня</label>
                <label><input class="uk-radio" type="radio" name="room" value="детская"> Детская</label>
                <label><input class="uk-radio" type="radio" name="room" value="кухня"> Кухня</label>
                <label><input class="uk-radio" type="radio" name="room" value="прихожая"> Прихожая</label>
                <label><input class="uk-radio" type="radio" name="room" value="санузел"> Санузел</label>
            </div>
            <div uk-grid class="uk-margin-small-top">
                <label><input class="uk-radio" type="radio" name="texture" value="матовые"> Матовые</label>
                <label><input class="uk-radio" type="radio" name="texture" value="глянцевые"> Глянцевые</label>
                <label><input class="uk-radio" type="radio" name="texture" value="сатиновые"> Сатиновые</label>
                <label><input class="uk-radio" type="radio" name="texture" value="дизайнерские"> Дизайнерские</label>
                <label for=""> | </label>
                <label><input class="uk-radio" type="radio" name="feature" value="с подсветкой"> С подсветкой</label>
                <label><input class="uk-radio" type="radio" name="feature" value="двухуровневые"> Двухуровневые</label>
                <label><input class="uk-radio" type="radio" name="feature" value="фотопечать"> Фотопечать</label>
            </div>
        </form>
    </div>

    <div class="uk-hidden@m">
        <div>
            <a href="#modal-center" uk-toggle><span class="js-filter--label">Все потолки</span> <span
                        uk-icon="triangle-down"></span></a>
        </div>

        <div id="modal-center" class="uk-flex-top" uk-modal>
            <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
                <button class="uk-modal-close-default" type="button" uk-close></button>
                <form class="js-ceiling-filter_form ceiling-filter_form-mobile">
                    <label><input class="uk-radio" type="radio" name="all" value="*" checked> Все потолки</label>
                    <hr>
                    <label><input class="uk-radio" type="radio" name="room" value="гостиная"> Гостиная</label>
                    <label><input class="uk-radio" type="radio" name="room" value="спальня"> Спальня</label>
                    <label><input class="uk-radio" type="radio" name="room" value="детская"> Детская</label>
                    <label><input class="uk-radio" type="radio" name="room" value="кухня"> Кухня</label>
                    <label><input class="uk-radio" type="radio" name="room" value="прихожая"> Прихожая</label>
                    <label><input class="uk-radio" type="radio" name="room" value="санузел"> Санузел</label>
                    <hr>
                    <label><input class="uk-radio" type="radio" name="texture" value="матовые"> Матовые</label>
                    <label><input class="uk-radio" type="radio" name="texture" value="глянцевые"> Глянцевые</label>
                    <label><input class="uk-radio" type="radio" name="texture" value="сатиновые"> Сатиновые</label>
                    <label><input class="uk-radio" type="radio" name="texture" value="дизайнерские"> Дизайнерские</label>
                    <hr>
                    <label><input class="uk-radio" type="radio" name="feature" value="с подсветкой"> С подсветкой</label>
                    <label><input class="uk-radio" type="radio" name="feature" value="двухуровневые"> Двухуровневые</label>
                    <label><input class="uk-radio" type="radio" name="feature" value="фотопечать"> Фотопечать</label>
                </form>
            </div>
        </div>
    </div>


  <? if (is_array($this->items) && count($this->items) > 0): ?>
      <div class="uk-margin-top">
        <?
        $layout = new JLayoutFile('list', JPATH_ROOT . '/components/com_photoreact/layouts');
        echo $layout->render([
          'items' => $this->items,
          'material' => $this->material
        ]);
        ?>
          <div id="gallery-loader"></div>
      </div>
  <? endif; ?>
</div>
