<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class PhotoreactViewPhotoreact extends JViewLegacy
{
  public $items;
  public $material = [
    'classic'         => 'МСД классик',
    'premium'         => 'МСД премиум',
    'premium_plus'    => 'МСД премиум+',
    'premium_color'   => 'МСД премиум цветной',
    'premium_factory' => 'МСД премиум фактурный',
    'pongs'           => 'Понгс 325',
    'descor'          => 'Дескор (Д премиум)',
    'clipso'          => 'Клипсо 495D',
  ];

  /**
   * @param null $tpl
   * @return mixed|void
   * @throws JsonException
   * @throws Exception
   */
  public function display($tpl = null)
  {
    $model = $this->getModel();
    $this->items = $model->getItems();

    if (count($errors = $this->get('Errors'))) {
      throw new Exception(implode("\n", $errors));
    }

    parent::display($tpl);
  }

}
