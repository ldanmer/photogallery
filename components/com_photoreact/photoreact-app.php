<?php

defined('_JEXEC') or die;
$this_url = JUri::base() . 'components/com_photoreact/';
$loader = require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/models/address.php';

$document = JFactory::getDocument();
JHtml::_('jquery.framework');

$document->addScript($this_url . 'bower_components/uikit/dist/js/uikit.min.js');
$document->addScript($this_url . 'bower_components/uikit/dist/js/uikit-icons.min.js');
$document->addStyleSheet($this_url . 'bower_components/uikit/dist/css/uikit.css');
$document->addStyleSheet($this_url . 'assets/css/style.css');
$document->addScript($this_url . 'assets/js/script.js');