<?php

defined("_JEXEC") or die("Restricted access");
jimport('joomla.application.component.controller');

class PhotoreactControllerAddress extends JControllerLegacy {
  public $material = [
    'classic'         => 'МСД классик',
    'premium'         => 'МСД премиум',
    'premium_plus'    => 'МСД премиум+',
    'premium_color'   => 'МСД премиум цветной',
    'premium_factory' => 'МСД премиум фактурный',
    'pongs'           => 'Понгс 325',
    'descor'          => 'Дескор (Д премиум)',
    'clipso'          => 'Клипсо 495D',
  ];

  protected $view_list = 'address';

  public function getModel($name = 'Address', $prefix = 'PhotoreactModel', $config = []) {
    $config['ignore_request'] = true;
    $model = parent::getModel($name, $prefix, $config);
    return $model;
  }

  public function loadOffset() {
    $model = new PhotoreactModelAddress();
    $offset = JRequest::getInt('offset');
    $items = $model->getItems($offset);
    $layout = new JLayoutFile('list', JPATH_ROOT . '/components/com_photoreact/layouts');
    echo $layout->render([
      'items' => $items,
      'material' => $this->material
    ]);
    die;
  }
}