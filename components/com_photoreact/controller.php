<?php
defined("_JEXEC") or die("Restricted access");

class PhotoreactController extends JControllerLegacy
{
  public function display($cachable = false, $urlparams = false){
    $document	= JFactory::getDocument();
    $vFormat = $document->getType();
    $vName   = $this->input->getCmd('view', 'photoreact');
    /** @var PhotoreactModelAddress $model */
    $model = $this->getModel('Address',  'PhotoreactModel');

    $view = $this->getView($vName, $vFormat);
    $view->setModel($model, true);
    // Push document object into the view.
    $view->document = $document;

    // Display the view
    $view->display();
  }
}