<?php

defined('_JEXEC') or die;

class PhotoreactModelAddress extends JModelList
{
  public function __construct($config = [])
  {
    parent::__construct($config);
  }

  protected function getListQuery(int $offset = 0, int $limit = 5) {
    $db = $this->getDbo();
    $query = $db->getQuery(true);
    $query->select('a.*')->from('#__photoreact AS a')
      //->where($db->quoteName('date') . '<= CURDATE()')
      ->order($db->quoteName('date') . ' DESC')
      ->setLimit($limit, $offset);
    return $query;
  }

  protected function getItemQuery(int $id){
    $db = $this->getDbo();
    $query = $db->getQuery(true);
    $query->select('a.*')->from('#__photoreact AS a')
      ->where($db->quoteName('id') . '=' . $id);
    return $query;
  }


  public function getItems($offset = 0, $limit = 5) {
    $db = JFactory::getDbo();
    $address_query = $this->getListQuery($offset, $limit);
    $db->setQuery($address_query);
    return $db->loadObjectList();
  }

  public function getItemById(int $id){
    $db = JFactory::getDbo();
    $query = $this->getItemQuery($id);
    $db->setQuery($query);
    return $db->loadObjectList();
  }


}